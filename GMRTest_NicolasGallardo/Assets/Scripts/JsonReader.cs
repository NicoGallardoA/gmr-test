﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using UnityEngine.UI;

public class JsonReader : MonoBehaviour
{
    [SerializeField] private Text titleText;

	[Header("Headers")]
	[SerializeField] private Transform headersRoot;
	[SerializeField] private Font headersFont;
	[SerializeField] private int headersFontSize;

	[Header("List Elements")]
	[SerializeField] private Transform TableElementsRoot;
	[SerializeField] private GridLayoutGroup lineLayoutGroupPrefab;
	[SerializeField] private Font tableFont;
	[SerializeField] private int tableFontSize;

	[Header("Error Message")]
	[SerializeField] private GameObject errorMessage;

	private List<string> headers;
	private string title;
	private List<List<string>> tableLines;

	public void Start()
	{
		LoadData();
	}

	public void LoadData()
	{
		ClearTable();
		CloseError();

		if (ReadJson())
			UpdateTable();
	}

	private void UpdateTable()
	{
		titleText.text = title;

		foreach (var header in headers)
		{
			AddElementToHeaders(header);
		}

		foreach(var line in tableLines)
		{
			AddNewLine(line);
		}
	}

	private void ClearTable()
	{
		DestroyChildren(headersRoot);
		DestroyChildren(TableElementsRoot);
	}

	private void DestroyChildren(Transform parent)
	{
		foreach (Transform child in parent)
		{
			GameObject.Destroy(child.gameObject);
		}
	}

	private void AddElementToHeaders(string text)
	{
		var newTextGameObject = new GameObject("Header");
		newTextGameObject.transform.parent = headersRoot;
		newTextGameObject.transform.localScale = Vector3.one;

		var newText = newTextGameObject.AddComponent<Text>();
		newText.text = text;
		newText.font = headersFont;
		newText.fontSize = headersFontSize;
	}

	private void AddNewLine(List<string> line)
	{
		var newLine = Instantiate(lineLayoutGroupPrefab, TableElementsRoot);
		
		foreach(var tableElement in line)
		{
			AddTableElement(tableElement, newLine.transform);
		}
	}

	private void AddTableElement(string text, Transform parent)
	{
		var newTextGameObject = new GameObject("Element");
		newTextGameObject.transform.parent = parent;
		newTextGameObject.transform.localScale = Vector3.one;

		var newText = newTextGameObject.AddComponent<Text>();
		newText.text = text;
		newText.font = tableFont;
		newText.fontSize = tableFontSize;
	}

	private bool ReadJson()
	{
		var path = Application.streamingAssetsPath + "/JsonChallenge.json";
		string contents = File.ReadAllText(path);

		try
		{
			IDictionary<string, JToken> jsonData = JObject.Parse(contents);
			OrganizeData(jsonData);

			return true;
		}
		catch
		{
			ShowError();

			return false;
		}
	}

	private void OrganizeData(IDictionary<string, JToken> jsonData)
	{
		foreach (KeyValuePair<string, JToken> element in jsonData)
		{
			string innerKey = element.Key;

			if (innerKey == "Title")
			{
				title = element.Value.ToString();
			}
			else if (innerKey == "ColumnHeaders")
			{
				headers = new List<string>();

				foreach (var header in element.Value)
				{
					headers.Add(header.ToString());
				}
			}
			else if (innerKey == "Data")
			{
				var data = element.Value;
				tableLines = new List<List<string>>();

				foreach (var dataLine in data)
				{
					var line = new List<string>();
					IDictionary<string, JToken> parsedDataLine = JObject.Parse(dataLine.ToString());

					foreach (var parsedData in parsedDataLine)
					{
						line.Add(parsedData.Value.ToString());
					}

					tableLines.Add(line);
				}
			}
		}
	}

	private void ShowError()
	{
		errorMessage.SetActive(true);
	}

	public void CloseError()
	{
		errorMessage.SetActive(false);
	}
}

[Serializable]
public class JsonData
{
	public string Title;
	public string[] ColumnHeaders;
	public LinesData[] Data;
}

[Serializable]
public class LinesData
{
	public string ID;
	public string Name;
	public string Role;
	public string Nickname;
}

[Serializable]
public class TableLines
{
	public string[] Line;
}
